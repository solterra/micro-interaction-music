import React,{ createContext, useState } from 'react';

const MusicContext = createContext();

const MusicContextProvider = ({ children }) => {
	const [selection, setSelection] = useState({startTime: 0, endTime: 0});
	return (
		<MusicContext.Provider value={{ selection, setSelection }}>
			{children}
		</MusicContext.Provider>
	);
};

export { MusicContext, MusicContextProvider };

