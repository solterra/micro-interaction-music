import { BrowserRouter, Routes, Route } from "react-router-dom";
import React, { useState, useMemo } from "react";
import AudioPlayer from "./AudioPlayer";
import ConsumerAudio from "./CosumerAudio";
import tracks from "./tracks";

function App() {
  
  return (
    <div className="App">
      <BrowserRouter>
           <Routes>
            <Route exact path="/" element={<AudioPlayer tracks={tracks} />} />
            <Route
              exact
              path="user"
              element={<ConsumerAudio tracks={tracks} />}
            />
          </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
