import React, { useContext, useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import AudioControls from "./AudioControls";
import "./styles.css";
import {MusicContext} from "./MusicContext";

//WaveSurfer Library 
import waveSurfer from "wavesurfer.js";
import RegionsPlugin from 'wavesurfer.js/dist/plugin/wavesurfer.regions.min.js';

const AudioPlayer = ({ tracks }) => {

  //Initialize Context
  const {setSelection} = useContext(MusicContext);

  // State
  const [trackIndex, setTrackIndex] = useState(0);
  const [trackProgress, setTrackProgress] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);

  //WaveForm State
  const wavesurferRef = useRef(null);

  // Create an instance of the wavesurfer
  const [wavesurferObj, setWavesurferObj] = useState();

  // Create Selected Region Start Stop
  const [waveRegion, setWaveregion] = useState({start: 0, end: 0}); 

  // Destructure for conciseness
  const { title, artist, color, image, audioSrc } = tracks[trackIndex];

  // Refs
  const audioRef = useRef(new Audio(audioSrc));
  const intervalRef = useRef();
  const isReady = useRef(false);

  //Regions


  // Destructure for conciseness
  const { duration } = audioRef.current;

  const currentPercentage = duration
    ? `${(trackProgress / duration) * 100}%`
    : "0%";
  const trackStyling = `
    -webkit-gradient(linear, 0% 0%, 100% 0%, color-stop(${currentPercentage}, #fff), color-stop(${currentPercentage}, #777))
  `;

  const startTimer = () => {
    // Clear any timers already running
    clearInterval(intervalRef.current);

    intervalRef.current = setInterval(() => {
      if (audioRef.current.ended) {
        toNextTrack();
      } else {
        setTrackProgress(audioRef.current.currentTime);
      }
    }, [1000]);
  };

  const onScrub = (value) => {
    // Clear any timers already running
    clearInterval(intervalRef.current);
    audioRef.current.currentTime = value;
    setTrackProgress(audioRef.current.currentTime);
  };

  const onScrubEnd = () => {
    // If not already playing, start
    if (!isPlaying) {
      setIsPlaying(true);
    }
    startTimer();
  };

  // create the waveform inside the correct component
  useEffect(() => {
    if (wavesurferRef.current && !wavesurferObj) {
      setWavesurferObj(
        waveSurfer.create({
          container: '#waveform',
          autoCenter: true,
          cursorColor: 'violet',
          loopSelection: true,
          waveColor: "#eee",
          progressColor: "yellow",
          cursorColor: "red",
          barWidth: 3,
          barRadius: 3,
          height: 75,
          responsive: true,
          // If true, normalize by the maximum peak instead of 1.0.
          normalize: true,
          // Use the PeakCache to improve rendering speed of large waveforms.
          partialRender: true,
          plugins: [
            RegionsPlugin.create({}),
          ],
        })
      );
    }
  }, [wavesurferRef, wavesurferObj]);

  	// once the file URL is ready, load the file to produce the waveform
	useEffect(() => {
		if (wavesurferObj) {
			wavesurferObj.load(audioSrc);
		}
	}, [wavesurferObj]);

  //Wave Surfer Actions
  useEffect(() => {
		if (wavesurferObj) {
			// once the waveform is ready, play the audio
			wavesurferObj.on('ready', () => {
				wavesurferObj.enableDragSelection({}); // to select the region to be trimmed
				});

			// once audio starts playing, set the state variable to true
			wavesurferObj.on('play', () => {
				setIsPlaying(true);
			});

			// once audio starts playing, set the state variable to false
			wavesurferObj.on('finish', () => {
				setIsPlaying(false);
			});

			// if multiple regions are created, then remove all the previous regions so that only 1 is present at any given time
			wavesurferObj.on('region-updated', (region) => {
				const regions = region.wavesurfer.regions.list;
				const keys = Object.keys(regions);
        if (keys.length > 1) {
					regions[keys[0]].remove();
				}
        let selectedRegion = regions[keys[0]]

        if (selectedRegion) {
          const start = selectedRegion.start;
          const end = selectedRegion.end;
          setWaveregion({start: start, end: end})
          setSelection({startTime: start, endTime: end})
        }
			});
		}
	}, [wavesurferObj]);

/*   const playRegion = () => {
    setIsPlaying(true)
    waveSurfer.current.play(region.start)
    onScrub(region.start)
  } */

  const clearRegion = () => {
    wavesurferObj.clearRegions()
    setIsPlaying(false);
    setWaveregion({start: 0, end: 0})
    setSelection({startTime: 0, endTime: 0})
  }

  const toPrevTrack = () => {
    if (trackIndex - 1 < 0) {
      setTrackIndex(tracks.length - 1);
    } else {
      setTrackIndex(trackIndex - 1);
    }
  };

  const toNextTrack = () => {
    if (trackIndex < tracks.length - 1) {
      setTrackIndex(trackIndex + 1);
    } else {
      setTrackIndex(0);
    }
  };

  useEffect(() => {
    if (isPlaying) {
      audioRef.current.play();
      startTimer();
    } else {
      audioRef.current.pause();
      //wavesurfer.current.pause();
    }
  }, [isPlaying]);

  // Handles cleanup and setup when changing tracks
  useEffect(() => {
    audioRef.current.pause();
    audioRef.current = new Audio(audioSrc);
    setTrackProgress(audioRef.current.currentTime);

    if (isReady.current) {
      audioRef.current.play();
      setIsPlaying(true);
      startTimer();
    } else {
      // Set the isReady ref as true for the next pass
      isReady.current = true;
    }
  }, [trackIndex]);

  useEffect(() => {
    // Pause and clean up on unmount
    return () => {
      audioRef.current.pause();
      clearInterval(intervalRef.current);
    };
  }, []);


  return (
    <>
      <div className="audio-player">
        <div className="track-info">
          <img
            className="artwork"
            src={image}
            alt={`track artwork for ${title} by ${artist}`}
          />
          <h2 className="title">{title}</h2>
          <h3 className="artist">{artist}</h3>
          <AudioControls
            isPlaying={isPlaying}
            onPrevClick={toPrevTrack}
            onNextClick={toNextTrack}
            onPlayPauseClick={setIsPlaying}
          />
          <input
            type="range"
            value={trackProgress}
            step="1"
            min="0"
            max={duration ? duration : `${duration}`}
            className="progress"
            onChange={(e) => onScrub(e.target.value)}
            onMouseUp={onScrubEnd}
            onKeyUp={onScrubEnd}
            style={{ background: trackStyling }}
          />
        </div>
      </div>
      <div>
      </div>
      <div id="waveform" ref={wavesurferRef} />
      <div className="button-bar">
        <button className="act-button title">
          Play Selected
        </button>
        <button onClick={clearRegion}  className="act-button title">
          Clear Region
        </button>
        <button className="act-button title">
          <Link to="/user">Switch to Consumer</Link>
        </button>
      </div>
      <div className="button-bar title">
        <p>
          Start : {waveRegion.start}
        </p>
        <p>
          End : {waveRegion.end}
        </p>
      </div>
    </>
  );
};

export default AudioPlayer;
